package com.qadeshafiq.writereaddb.modal;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "USERS_EXPERIENCE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ExperienceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long regNo;

    @OneToOne
    @JoinColumn
    @MapsId
    private ConsultantEntity consultant;

    @Column(name = "Java")
    private String java;

    @Column(name = "CPlusPlus")
    private String cPlusPlus;

    @Column(name = "CSharp")
    private String cSharp;

    @Column(name = "MATLAB")
    private String matlab;

    @Column(name = "Angular")
    private String angular;

}
