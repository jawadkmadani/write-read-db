package com.qadeshafiq.writereaddb.modal;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "CONSULTANTS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConsultantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long regNo;

    @Column(name = "Name")
    private String name;

    @Column(name = "Subject")
    private String subject;

    @Column(name = "Client")
    private String client;

    @Column(name = "Marks")
    private double marks;

    @OneToOne(mappedBy = "consultant", cascade = CascadeType.ALL)
    private ExperienceEntity experience;

}
