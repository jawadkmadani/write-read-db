package com.qadeshafiq.writereaddb.controller;

import com.qadeshafiq.writereaddb.modal.ConsultantEntity;
import com.qadeshafiq.writereaddb.service.ConsultantService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ConsultantController {

    private final ConsultantService service;

    public ConsultantController(ConsultantService service) {
        this.service = service;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<ConsultantEntity> saveToDb(@RequestBody ConsultantEntity entity) {

        return new ResponseEntity<>(service.addToDb(entity), HttpStatus.OK);
    }


    @GetMapping(path = "/find/{subject}")
    public ResponseEntity findBySubjectInDb(@PathVariable("subject") String subject) {

        return new ResponseEntity<>(service.findInDb(subject), HttpStatus.OK);
    }

    @GetMapping(path = "/find/{subject}/{name}")
    public ResponseEntity findBySubjectInDb(@PathVariable("subject") String subject,
                                            @PathVariable("name") String name) {

        return new ResponseEntity<>(service.findAllBySubjectAndNameInDb(subject, name), HttpStatus.OK);
    }


    @GetMapping(path = "/find/all")
    public ResponseEntity findAllinDb() {

        return new ResponseEntity<>(service.findAllInDb(), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{name}")
    public ResponseEntity deleteByNameInDb(@PathVariable("name") String name) {
        service.deleteByName(name);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
