package com.qadeshafiq.writereaddb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WriteReadDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(WriteReadDbApplication.class, args);
    }

}
