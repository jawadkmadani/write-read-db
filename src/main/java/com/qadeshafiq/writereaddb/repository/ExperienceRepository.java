package com.qadeshafiq.writereaddb.repository;

import com.qadeshafiq.writereaddb.modal.ExperienceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExperienceRepository extends JpaRepository<ExperienceEntity, Long> {
}
