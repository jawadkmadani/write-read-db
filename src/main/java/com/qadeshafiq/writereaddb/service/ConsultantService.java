package com.qadeshafiq.writereaddb.service;

import com.qadeshafiq.writereaddb.modal.ConsultantEntity;
import com.qadeshafiq.writereaddb.repository.ConsultantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsultantService {

    private final ConsultantRepository repository;

    public ConsultantService(ConsultantRepository repository) {
        this.repository = repository;
    }

    public ConsultantEntity addToDb(ConsultantEntity entity) {
        return repository.save(entity);
    }

    public List<ConsultantEntity> findInDb(String subject) {
        return repository.findBySubject(subject);
    }

    public List<ConsultantEntity> findAllBySubjectAndNameInDb(String subject, String name) {
        return repository.findBySubjectAndName(subject, name);
    }

    public List<ConsultantEntity> findAllInDb() {
        return repository.findAll();
    }


    public void deleteByName(String name) {
        repository.deleteByName(name);
    }
}
